﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Comm;
using ExcelDna.Integration;
using System.IO;

namespace ExcelStratPositions
{
    public static class ExcelOrderSender2
    {
        static int bStarted = 0;
        static Communication objCommunication;
        internal static BlockingCollection<byte[]> goClientQueue = new BlockingCollection<byte[]>();
        internal static System.Threading.ManualResetEvent goManualResetEvent = new System.Threading.ManualResetEvent(false);
        internal static string DealerId = "";
        public static Dictionary<string, long> SymbolToTokenDict = new Dictionary<string, long>();
        public static Dictionary<long, string> TokenToSymbolDict = new Dictionary<long, string>();
        public static DateTime fromTime = new DateTime(1980, 1, 1, 0, 0, 0);
        static string Formatstr = "ddMMMyyyy";
        static string Formatstr1 = "0.00";
        static long lnTime;
        public static bool bRELogin = false;
        public static bool bGWLogin = false;

        internal static NetPosition oNetPosition = null;//= new NetPosition();


        [ExcelFunction(Description = "Function to start the communication")]
        public static object Start()
        {
            int i=  1;
            Logger.WriteToLog("Start Function");
           
            if (bStarted == 0)
            {
              
                objCommunication = new Communication();
             
                try
                {

                    MyXMLSettings mXMLSettings = new MyXMLSettings();

                    Logger.WriteToLog("Loading Contracts Start");
                    string strSymbol;
                    foreach (string s in File.ReadAllLines(".\\contract.txt"))
                    {
                        strSymbol = "";
                        var vals = s.Split('|');
                        if (vals.Count() > 7)
                        {
                            if (String.IsNullOrEmpty(vals[2]))
                                continue;
                            else if (vals[2].Substring(0, 1) == "O")
                                strSymbol = vals[2] + "-" + vals[3] + "-" + FormatExpiry(vals[6]).ToUpper() + "-" + FormatStrike(vals[7]) + "-" + vals[8];
                            else if (vals[2].Substring(0, 1) == "F")
                                strSymbol = vals[2] + "-" + vals[3] + "-" + FormatExpiry(vals[6]).ToUpper();

                            //  if (vals[2] == "EQ")
                            {
                                SymbolToTokenDict[strSymbol] = Convert.ToInt32(vals[0]);
                                TokenToSymbolDict[Convert.ToInt32(vals[0])] = strSymbol;
                                //if (Convert.ToInt32(vals[0]) > 39900 && Convert.ToInt32(vals[0]) < 39969)
                                //{
                                //    System.IO.File.AppendAllText("selectcontract", strSymbol);
                                //}
                            }
                        }
                    }

                    Logger.WriteToLog("Loading Contracts End");
                    Logger.WriteToLog("Connecting to Server");
                  //  return i;
                    if (objCommunication.Connect(2, mXMLSettings.strServerPort, mXMLSettings.strServerIP, mXMLSettings.iLog, ServerReceiver) != -1)
                    {

                        System.Threading.Thread t = new System.Threading.Thread(() => DequeueThread());
                        t.Start();

                        bStarted = 1;
                        Logger.WriteToLog("Connected to Server");
                        SSTCryptographer.Key = "Wonderful";
                        string temp = SSTCryptographer.Encrypt(mXMLSettings.strPassword);
                        Login(mXMLSettings.strLogin, temp);
                        Logger.WriteToLog("Sent Login Request");
                        DealerId = mXMLSettings.strLogin;
                    }
                    else
                        Logger.WriteToLog("Connecting to Server Failed");

                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.ToString());
                    return 9;

                }

            }
            else if (bRELogin == false)
            {
                oNetPosition.Clear();
                MyXMLSettings mXMLSettings = new MyXMLSettings();
                SSTCryptographer.Key = "Wonderful";
                string temp = SSTCryptographer.Encrypt(mXMLSettings.strPassword);
                Login(mXMLSettings.strLogin, temp);
                Logger.WriteToLog("Sent Login Request");
                DealerId = mXMLSettings.strLogin;
            }
            return 5;
        }

        public static string FormatExpiry(string s)
        {

            lnTime = Convert.ToInt32(s);
            //DateTime t = new DateTime();
            DateTime t = fromTime.AddSeconds(lnTime);
            return t.ToString(Formatstr);


        }
        public static string FormatStrike(string s)
        {
            double lnTime = Convert.ToDouble(s);
            return (lnTime / 100.00).ToString(Formatstr1);
        }

        [ExcelFunction(Description = "Function to start the communication")]
        public static void Stop()
        {
            Logger.WriteToLog("Sent Logout Request");
            string strLogOut = "LOUT|" + DealerId;
            byte[] LoginBuffer = Encoding.UTF8.GetBytes(strLogOut.ToString());
            objCommunication.SendToServer(LoginBuffer);
            bRELogin = false;
        }

        public static int Login(string ID, string pwd)
        {

            string strLogin = "LIN|" + ID + "|" + pwd;
            byte[] LoginBuffer = Encoding.UTF8.GetBytes(strLogin.ToString());
            objCommunication.SendToServer(LoginBuffer);


            return 1;
        }

        public static void SendOrderDownLoadRequest()
        {
            Logger.WriteToLog("Sent Order Download Request");
            string strLogin = "CODE=ODWNLOAD|USER=" + DealerId + "|LASTUPDATETIME=0|";
            byte[] LoginBuffer = Encoding.UTF8.GetBytes(strLogin.ToString());
            objCommunication.SendToServer(LoginBuffer);

        }

        public static void SendTradeDownLoadRequest()
        {
            Logger.WriteToLog("Sent Trade Download Request");
            string strLogin = "CODE=ETRADEDNLD|USER=" + DealerId + "|LASTUPDATETIME=0|";
            byte[] LoginBuffer = Encoding.UTF8.GetBytes(strLogin.ToString());
            objCommunication.SendToServer(LoginBuffer);

        }

        public static void SendGatewayStatusRequest()
        {
            Logger.WriteToLog("Sent Gateway Status Request");
            string strLogin = "CODE=OGWLOGINDWNLOAD";
            byte[] LoginBuffer = Encoding.UTF8.GetBytes(strLogin.ToString());
            objCommunication.SendToServer(LoginBuffer);

        }

        public static bool ServerReceiver(byte[] Message, int port)
        {

            if (Message != null)
            {

                goClientQueue.Add(Message);
                goManualResetEvent.Set();

            }


            return true;
        }


        public static void DequeueThread()
        {
            while (true)
            {
                goManualResetEvent.WaitOne();

                while (goClientQueue.Count > 0)
                {
                    byte[] loClientData;
                    goClientQueue.TryTake(out loClientData);
                    if (loClientData != null)
                        ProcessServerData(loClientData);

                }
                goManualResetEvent.Reset();
            }
        }

        public static bool ProcessServerData(byte[] Message)
        {
            try
            {
                if (Message == null)
                {
                    bStarted = 0;
                    bRELogin = false;
                }
                else
                {
                    string sMessage = Encoding.UTF8.GetString(Message);

                    string[] sMessageArray1 = sMessage.Split('~');
                    int nLength = sMessageArray1[0].Length;

                    byte[] b = null;
                    if (sMessageArray1.Count() > 1)
                    {
                        b = new byte[Message.Length - 1 - nLength];
                        Buffer.BlockCopy(Message, nLength + 1, b, 0, Message.Length - 1 - nLength);
                    }


                    Logger.WriteToLog(sMessageArray1[0]);
                    string[] sMessageArray = sMessageArray1[0].Split('|');
                    string strCode = sMessageArray[0];
                    if (sMessageArray[0].IndexOf('=') != -1)
                    {
                        strCode = sMessageArray[0].Substring(sMessageArray[0].IndexOf('=') + 1);
                    }
                    switch (strCode)
                    {
                        case "OE":
                        case "OM":
                        case "OC":
                        case "ROE":
                        case "EOE":
                        case "ROM":
                        case "EOM":
                        case "ROC":
                        case "EOC":
                        case "OCONF":
                        case "OERR":
                        case "OMCONF":
                        case "OMERR":
                        case "OCCONF":
                        case "OCERR":
                        case "OSLTRIG":
                        case "OTRADE":
                            {
                                OrderProcessor.ProcessOrder(b);
                                break;
                            }
                        case "RLIN":
                            {
                                bRELogin = true;
                                Logger.WriteToLog("Logged In");

                                SendOrderDownLoadRequest();
                            }
                            break;
                        case "OORDDWNLOAD":
                            {
                                OrderProcessor.ProcessOrderDownload(b);
                                SendTradeDownLoadRequest();
                                SendGatewayStatusRequest();
                            }
                            break;
                        case "OTRADEDNLD":
                            {
                                string[] strTradeArray = sMessage.Split('#');
                                string[] strMessageArray;
                                foreach (string str in strTradeArray)
                                {
                                    strMessageArray = str.Split('|');
                                    Trade lTrade = new Trade();
                                    if (lTrade.FillTrade(ref strMessageArray) == true)
                                    {


                                        oNetPosition.UpdateNetPosOnTrade(ref lTrade);
                                    }
                                }
                            }
                            break;
                        case "OGWLOGIN":
                            bGWLogin = true;
                            break;
                        case "OGWLOGOFF":
                            bGWLogin = false;
                            break;

                    }

                    if (strCode == "OTRADE")
                    {
                        Trade lTrade = new Trade();
                        if (lTrade.FillTrade(ref sMessageArray) == true)
                        {


                            oNetPosition.UpdateNetPosOnTrade(ref lTrade);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
            return true;
        }

        [ExcelFunction(Description = "Function to send the order")]
        public static void SendOrder(string name)
        {
            if (bStarted != 0)
            {
                Logger.WriteToLog("Sent Order " + name);
                byte[] b = System.Text.Encoding.UTF8.GetBytes(name);
                objCommunication.SendToServer(b);
            }
        }




    }
}
