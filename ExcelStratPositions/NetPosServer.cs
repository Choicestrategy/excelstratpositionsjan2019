﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelDna;
using ExcelDna.Integration.Rtd;
using ExcelDna.Integration;
using System.Collections.Concurrent;
using System.Threading;
using System.IO;

namespace ExcelStratPositions
{
    
       
        public class NetPosServer : ExcelRtdServer
        {
            private readonly List<Topic> topics = new List<Topic>();
            // private Dictionary<string, List<Topic>> lTopicDict = new Dictionary<string, List<Topic>>();
            internal static Dictionary<string, Dictionary<string, List<Topic>>> lstSymbolWiseDict = new Dictionary<string, Dictionary<string, List<Topic>>>();

            public static DateTime fromtime1 = new DateTime(1980, 1, 1, 0, 0, 0);
            private Dictionary<Topic, IList<string>> lstTopicData = new Dictionary<Topic, IList<string>>();

            internal static ConcurrentQueue<NetPosData> goNSEQueue = new ConcurrentQueue<NetPosData>();
            internal static System.Threading.ManualResetEvent goNSEManualResetEvent = new System.Threading.ManualResetEvent(false);

            public static bool bIsInitialized = false;
            public NetPosServer()
            {
            // timer = new System.Threading.Timer(Callback);
            // m_callback = callback;
            // timer.Enabled = true;
            //  timer.AutoReset = true;
            // timer.Elapsed
            if (ExcelStratPositions2.SymbolToTokenDict.Count <= 0)
            {
                string strSymbol;
                foreach (string s in File.ReadAllLines(".\\contract.txt"))
                {
                    strSymbol = "";
                    var vals = s.Split('|');
                    if (vals.Count() > 7)
                    {
                        if (String.IsNullOrEmpty(vals[2]))
                            continue;
                        else if (vals[2].Substring(0, 1) == "O")
                            strSymbol = vals[2] + "-" + vals[3] + "-" + ExcelStratPositions2.FormatExpiry(vals[6]).ToUpper() + "-" + ExcelStratPositions2.FormatStrike(vals[7]) + "-" + vals[8];
                        else if (vals[2].Substring(0, 1) == "F")
                            strSymbol = vals[2] + "-" + vals[3] + "-" + ExcelStratPositions2.FormatExpiry(vals[6]).ToUpper();

                        //  if (vals[2] == "EQ")
                        {
                            ExcelStratPositions2.SymbolToTokenDict[strSymbol] = Convert.ToInt32(vals[0]);
                            ExcelStratPositions2.TokenToSymbolDict[Convert.ToInt32(vals[0])] = strSymbol;
                            //if (Convert.ToInt32(vals[0]) > 39900 && Convert.ToInt32(vals[0]) < 39969)
                            //{
                            //    System.IO.File.AppendAllText("selectcontract", strSymbol);
                            //}
                        }
                    }
                }
            }

            Thread t1 = new System.Threading.Thread(() => NSEDequeueThread());
                    t1.Start();

                    NetPosition.RegisterCallBack(Callback);               
                    

            }

            private void Callback(string Symbol, NetPosData o)
            {
                goNSEQueue.Enqueue(o);
                goNSEManualResetEvent.Set();
                return;
            }


            public static void NSEDequeueThread()
            {
                while (true)
                {
                    goNSEManualResetEvent.WaitOne();

                    while (goNSEQueue.Count > 0)
                    {
                        NetPosData loClientData;
                        goNSEQueue.TryDequeue(out loClientData);
                        if (loClientData != null)
                        {
                            //ProcessNSEData(loClientData.byClientMessage, loClientData.port, loClientData.bCompress, loClientData.NoOfMessages);
                            ProcessNP(ref loClientData);

                        }
                        else
                        {

                        }
                    }
                    goNSEManualResetEvent.Reset();
                }
            }

        private static void ProcessNP(ref NetPosData o)
        {
            try
            {
                // Stop();
                List<Topic> topics1;
                Dictionary<string, List<Topic>> lTopicDict;
                string str = "";
                if (string.IsNullOrEmpty(o._strat))
                 str = o._symbol + "@" + o._clientId + "@";
                else
                 str = o._symbol + "@" + o._clientId + "@" + o._strat;
                //List<string> strList = new List<string>();
              //  strList.Add(str2);
               // strList.Add(str1);


                //foreach (string str in strList)
                {
                    if (lstSymbolWiseDict.TryGetValue(str, out lTopicDict))
                    {
                        foreach (string topicId in lTopicDict.Keys)
                        {
                            if (lTopicDict.TryGetValue(topicId, out topics1))
                            {
                                switch (topicId)
                                {
                                    case "TotalBuyQty":
                                        {
                                            foreach (Topic topic in topics1)
                                            {
                                                topic.UpdateValue(o._buyqty);
                                            }
                                        }
                                        break;
                                    case "TotalSellQty":
                                        {
                                            foreach (Topic topic in topics1)
                                            {
                                                topic.UpdateValue(o._sellqty);
                                            }
                                        }
                                        break;
                                    case "TotalBuyValue":
                                        {
                                            foreach (Topic topic in topics1)
                                            {
                                                topic.UpdateValue(o._buyvalue / 100.00);
                                            }
                                        }
                                        break;
                                    case "TotalSellValue":
                                        {
                                            foreach (Topic topic in topics1)
                                            {

                                                topic.UpdateValue(o._sellvalue / 100.00);
                                            }
                                        }
                                        break;



                                }
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
        }


            protected override void ServerTerminate()
            {
                //  timer.Dispose();
                //timer = null;
            }

        public void UpdateInitialData(string strSymbol, string strClient, Topic topic, string topicId, string strStrat)
        {
            try
            {
                NetPosData o = new NetPosData();
                Logger.WriteToLog("UpdateInitialData");
                if (ExcelStratPositions2.oNetPosition.dictClientWiseNetPositions == null)
                    return;

                Logger.WriteToLog("1");
                ClientNetPos lClientNetPos;
                while (true)
                {
                    if (string.IsNullOrEmpty(strClient))
                    {
                        OrderProcessor.lStratClientDict.TryGetValue(strStrat, out strClient);
                    }

                    if (string.IsNullOrEmpty(strClient))
                        return;
                    if (!ExcelStratPositions2.oNetPosition.dictClientWiseNetPositions.TryGetValue(strClient, out lClientNetPos))
                    {
                        lClientNetPos = new ClientNetPos();
                        ExcelStratPositions2.oNetPosition.dictClientWiseNetPositions.Add(strClient, lClientNetPos);
                    }

                    Logger.WriteToLog("2");
                    if (lClientNetPos.dictContractWiseNetPositions == null)
                        return;

                    Logger.WriteToLog("3");
                    long nToken;
                    if (!ExcelStratPositions2.SymbolToTokenDict.TryGetValue(strSymbol, out nToken))
                    {
                        return;
                    }

                    Logger.WriteToLog("4");
                    Int32 Token = Convert.ToInt32(nToken);

                    if (string.IsNullOrEmpty(strStrat))
                    {


                        if (!lClientNetPos.dictContractWiseNetPositions.TryGetValue(Token, out o))
                        {
                            o = new NetPosData();
                            lClientNetPos.dictContractWiseNetPositions.Add(Token, o);
                        }
                            
                    }
                    else
                    {
                        StratNetPos lStratNetPos;
                        if (!lClientNetPos.dictStratWiseNetPositions.TryGetValue(strStrat, out lStratNetPos))
                        {
                            lStratNetPos = new StratNetPos();
                            lClientNetPos.dictStratWiseNetPositions.Add(strStrat, lStratNetPos);
                        }

                        Logger.WriteToLog("5");
                        if (lStratNetPos.dictContractWiseNetPositions == null)
                            return;
                        Logger.WriteToLog("6");

                        if (!lStratNetPos.dictContractWiseNetPositions.TryGetValue(Token, out o))
                        {
                            o = new NetPosData();
                            lStratNetPos.dictContractWiseNetPositions.Add(Token, o);
                        }
                            break;

                        Logger.WriteToLog("7");


                    }
                    break;
                }

                

                switch (topicId)
                {
                    case "TotalBuyQty":
                        {

                            topic.UpdateValue(o._buyqty);

                        }
                        break;
                    case "TotalSellQty":
                        {

                            topic.UpdateValue(o._sellqty);

                        }
                        break;
                    case "TotalBuyValue":
                        {

                            topic.UpdateValue(o._buyvalue / 100.00);

                        }
                        break;
                    case "TotalSellValue":
                        {

                            topic.UpdateValue(o._sellvalue / 100.00);

                        }
                        break;

                }

            }
            catch (Exception e)
            {

                Logger.WriteToLog(e.ToString());
            }

        }
            

            protected override object ConnectData(Topic topic, IList<string> topicInfo, ref bool newValues)
            {
                try
                {

                lstTopicData.Add(topic, topicInfo);
                    Dictionary<string, List<Topic>> lTopicDict;

                    string str = topicInfo[1] + "@" + topicInfo[2] +"@" + topicInfo[3]; //symbol@client@strat
                    UpdateInitialData(topicInfo[1], topicInfo[2], topic, topicInfo[0],topicInfo[3]);

                    if (lstSymbolWiseDict.TryGetValue(str, out lTopicDict))
                    {


                    if (lTopicDict.ContainsKey(topicInfo[0]))
                    {
                        
                        {
                            ((IRtdServer)this).DisconnectData(Convert.ToInt32(topicInfo[0]));
                        }
                    }

                    List<Topic> topics1;
                        if (lTopicDict.TryGetValue(topicInfo[0], out topics1))
                        {
                            topics1.Add(topic);
                        }
                        else
                        {
                            topics1 = new List<Topic>();
                            topics1.Add(topic);
                            lTopicDict.Add(topicInfo[0], topics1);
                        }
                    }
                    else
                    {
                        List<Topic> topics1;
                        topics1 = new List<Topic>();
                        topics1.Add(topic);

                        lTopicDict = new Dictionary<string, List<Topic>>();
                        lTopicDict.Add(topicInfo[0], topics1);
                        lstSymbolWiseDict.Add(str, lTopicDict);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.ToString());
                }
                //  Start();
                // return GetTime();

                return "0";
            }

            protected override void DisconnectData(Topic topic)
            {
                try
                {
                    IList<string> topicInfo;
                    long nToken;


                    if (lstTopicData.TryGetValue(topic, out topicInfo))
                    {
                        string str = topicInfo[1] + "@" + topicInfo[2] + "@" + topicInfo[3]; //symbol@client
                        Dictionary<string, List<Topic>> lTopicDict;
                        if (lstSymbolWiseDict.TryGetValue(str, out lTopicDict))
                        {
                            List<Topic> topics1;
                            if (lTopicDict.TryGetValue(topicInfo[0], out topics1))
                            {
                                topics1.Remove(topic);
                            }

                            if (topics1.Count == 0)
                            {
                                lTopicDict.Remove(topicInfo[0]);
                            }

                            if (lTopicDict.Count == 0)
                            {
                                lstSymbolWiseDict.Remove(str);
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.ToString());
                }

                // topics.Remove(topic);
                // if (topics.Count == 0)
                //Stop();
            }



        }

        public static class NetPosfunctions
        {
            [ExcelFunction("Gets realtime values from server")]
            public static object GetStratNetPos(string S2, string S1,string S3, string S4)
            {
                if (String.IsNullOrEmpty(S2) || String.IsNullOrEmpty(S1) || (String.IsNullOrEmpty(S3) && String.IsNullOrEmpty(S4)))
                    return "";

         
        
                try
            {
                return XlCall.RTD("ExcelStratPositions.NetPosServer", null, S2, S1, S3, S4);
            }
            catch(Exception e)
            {

            }
            return 0;
            }


        }
    
        

}
