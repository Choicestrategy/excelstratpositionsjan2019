﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelDna.Integration;
using ExcelDna.Integration.Rtd;

namespace ExcelStratPositions
{
    public class OrderServer : ExcelRtdServer
    {
        private readonly List<Topic> topics = new List<Topic>();
        private Dictionary<string, Dictionary<string, List<Topic>>> lstOIDWiseDict = new Dictionary<string, Dictionary<string, List<Topic>>>();


        public static DateTime fromtime1 = new DateTime(1980, 1, 1, 0, 0, 0);
        private Dictionary<Topic, IList<string>> lstTopicData = new Dictionary<Topic, IList<string>>();

        public OrderServer()
        {
            OrderProcessor.RegisterCallback(Callback);
        }
       
        //Program.lDataFetcher.RegisterCallBack(Callback);


        private void Callback(string sOrderId, TradeSystemCommon.OrderData o)
        {
            ProcessOData(ref o);
            
        }

        private void ProcessOData(ref TradeSystemCommon.OrderData o)
        {
            try
            {
                // Stop();
                List<Topic> topics1;
                Dictionary<string, List<Topic>> lTopicDict;

                if (lstOIDWiseDict.TryGetValue(o.InternalOrderId, out lTopicDict))
                {
                    foreach (string topicId in lTopicDict.Keys)
                    {
                        if (lTopicDict.TryGetValue(topicId, out topics1))
                        {
                            switch (topicId)
                            {
                                case "TOTALORDERQTY":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.Qty);
                                        }
                                    }
                                    break;
                                case "PENDINGQTY":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.PendingQty);
                                        }
                                    }
                                    break;
                                case "STATUS":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.Status);
                                        }
                                    }
                                    break;
                                case "REASON":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.Reason);
                                        }
                                    }
                                    break;
                                case "ORDERPRICE":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.Price / 100.00);
                                        }
                                    }
                                    break;
                                case "EXCHANGEORDERNO":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.ExchangeOrderNo);
                                        }
                                    }
                                    break;
                                case "SIDE":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.BuySell);
                                        }
                                    }
                                    break;
                                case "TOTALTRADEDQTY":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.TotalTradedQty);
                                        }
                                    }
                                    break;
                                case "VALIDITY":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.Validity);
                                        }
                                    }
                                    break;
                                case "ORDERTYPE":
                                    {
                                        foreach (Topic topic in topics1)
                                        {
                                            topic.UpdateValue(o.OrderType);
                                        }
                                    }
                                    break;




                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }

        }



        protected override void ServerTerminate()
        {
            //  timer.Dispose();
            //timer = null;
        }


        public void UpdateInitialData(string  lOrderId, Topic topic, string topicId)
        {
            try
            {
                TradeSystemCommon.OrderData o;
                if (OrderProcessor.lOrderDict == null)
                    return;
                if (!OrderProcessor.lOrderDict.TryGetValue(lOrderId, out o))
                    return;
                switch (topicId)
                {
                    case "TOTALORDERQTY":
                        {
                            // foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.Qty);
                            }
                        }
                        break;
                    case "PENDINGQTY":
                        {
                            //foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.PendingQty);
                            }
                        }
                        break;
                    case "STATUS":
                        {
                            //foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.Status);
                            }
                        }
                        break;
                    case "REASON":
                        {
                            // foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.Reason);
                            }
                        }
                        break;
                    case "ORDERPRICE":
                        {
                            //foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.Price / 100.00);
                            }
                        }
                        break;
                    case "EXCHANGEORDERNO":
                        {
                            //foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.ExchangeOrderNo);
                            }
                        }
                        break;
                    case "SIDE":
                        {
                            // foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.BuySell);
                            }
                        }
                        break;
                    case "TOTALTRADEDQTY":
                        {
                            //  foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.TotalTradedQty);
                            }
                        }
                        break;
                    case "VALIDITY":
                        {
                            // foreach (Topic topic in topics1)
                            {
                                topic.UpdateValue(o.Validity);
                            }
                        }
                        break;
                    case "ORDERTYPE":
                        {
                            // foreach (Topic topic in topics1)
                            {

                                topic.UpdateValue(o.OrderType);
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }

        }

        protected override object ConnectData(Topic topic, IList<string> topicInfo, ref bool newValues)
        {
            try
            {
                lstTopicData.Add(topic, topicInfo);
                Dictionary<string, List<Topic>> lTopicDict;

                //if (!Program.SymbolToTokenDict.TryGetValue(topicInfo[1], out nToken))
                //return "0";

                UpdateInitialData(topicInfo[1], topic, topicInfo[0]);

                if (lstOIDWiseDict.TryGetValue(topicInfo[1], out lTopicDict))
                {
                    List<Topic> topics1;
                    if (lTopicDict.TryGetValue(topicInfo[0], out topics1))
                    {
                        topics1.Add(topic);
                    }
                    else
                    {
                        topics1 = new List<Topic>();
                        topics1.Add(topic);
                        lTopicDict.Add(topicInfo[0], topics1);
                    }
                }
                else
                {
                    List<Topic> topics1;
                    topics1 = new List<Topic>();
                    topics1.Add(topic);

                    lTopicDict = new Dictionary<string, List<Topic>>();
                    lTopicDict.Add(topicInfo[0], topics1);
                    lstOIDWiseDict.Add(topicInfo[1], lTopicDict);
                }

                //  Start();
                // return GetTime();
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
            return "0";
        }


        protected override void DisconnectData(Topic topic)
        {

            try
            {
                IList<string> topicInfo;


                if (lstTopicData.TryGetValue(topic, out topicInfo))
                {

                    Dictionary<string, List<Topic>> lTopicDict;
                    if (lstOIDWiseDict.TryGetValue(topicInfo[1], out lTopicDict))
                    {
                        List<Topic> topics1;
                        if (lTopicDict.TryGetValue(topicInfo[0], out topics1))
                        {
                            topics1.Remove(topic);
                        }

                        if (topics1.Count == 0)
                        {
                            lTopicDict.Remove(topicInfo[0]);
                        }

                        if (lTopicDict.Count == 0)
                        {
                            lstOIDWiseDict.Remove(topicInfo[1]);
                        }

                    }
                }


                // topics.Remove(topic);
                // if (topics.Count == 0)
                //Stop();
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
        }
        
      
    }

    public static class Orderfunctions
    {
        [ExcelFunction("Gets realtime values from server")]
        public static object GetOrderNew(string S2, string S1)
        {
            if (String.IsNullOrEmpty(S2) || String.IsNullOrEmpty(S1))
                return "";


            return XlCall.RTD("ExcelStratPositions.OrderServer", null, S2, S1);
        }


    }
}
