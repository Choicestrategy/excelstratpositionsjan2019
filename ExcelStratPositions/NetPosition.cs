﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ExcelStratPositions
{

    public class NetPosData 
    {
       

        public string _clientId;
        public string _symbol;
        public Int32 _token;
        public Int32 _buyqty;
        public Int32 _sellqty;
        public Int32 _netqty;
        public double _netvalue;
        public double _buyvalue;
        public double _sellvalue;
        public double _buyavgprice;
        public double _sellavgprice;
        public double _netprice;
        public string _strat;
            
        internal void UpdateOnTrade(ref Trade lTrade, bool bClient, bool bStrat)
        {
            try
            {
                _clientId = lTrade.lTradeData.ClientId;
                if (!bClient)
                {

                    _token = lTrade.lTradeData.Token;
                    string strSymbol;
                    ExcelStratPositions2.TokenToSymbolDict.TryGetValue(lTrade.lTradeData.Token, out strSymbol);
                    _symbol = strSymbol;
                }
                else
                    _token = -1;
                if (lTrade.lTradeData.BuySell == "B")
                {
                    _buyqty += lTrade.lTradeData.Qty;
                    _buyvalue += (lTrade.lTradeData.Price / 100.00) * lTrade.lTradeData.Qty;
                    _buyavgprice = _buyvalue / _buyqty;
                }
                else if (lTrade.lTradeData.BuySell == "S")
                {
                    _sellqty += lTrade.lTradeData.Qty;
                    _sellvalue += (lTrade.lTradeData.Price / 100.00) * lTrade.lTradeData.Qty;
                    _sellavgprice = _sellvalue / _sellqty;
                }

                _netqty = (_buyqty - _sellqty);
                _netvalue = Math.Abs(_sellvalue - _buyvalue);
                if (_netqty != 0)
                    _netprice = _netvalue / _netqty;


                if(bStrat)
                _strat = lTrade.lTradeData.Remarks;
              
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
        }

        

    }
    internal class ClientNetPos
    {
        NetPosData lClientNetPosData;
        public Dictionary<Int32, NetPosData> dictContractWiseNetPositions;
        public Dictionary<string, StratNetPos> dictStratWiseNetPositions;
       // public delegate void NetPosMessageHandler(ref NetPosData pNetPosData);
      //  public static event NetPosMessageHandler NewNetPosArrived;

        internal ClientNetPos()
        {
            lClientNetPosData = new NetPosData();
            dictContractWiseNetPositions = new Dictionary<Int32, NetPosData>();
            dictStratWiseNetPositions = new Dictionary<string, StratNetPos>();
        }
        internal NetPosData UpdateOnTrade(ref Trade lTrade, ref NetPosData lStratNetPosData)
        {
             NetPosData lContractNetPosData = null;
            StratNetPos lStratNetPos = null;
            try
            {
                lClientNetPosData.UpdateOnTrade(ref lTrade, true, false);
               
                if (dictContractWiseNetPositions.TryGetValue(lTrade.lTradeData.Token, out lContractNetPosData))
                {
                    lContractNetPosData.UpdateOnTrade(ref lTrade, false,false);
                }
                else
                {
                    lContractNetPosData = new NetPosData();
                    lContractNetPosData.UpdateOnTrade(ref lTrade, false,false);
                    dictContractWiseNetPositions.Add(lTrade.lTradeData.Token, lContractNetPosData);
                 //   if (NewNetPosArrived != null)
                  //  {
                  //      NewNetPosArrived(ref lContractNetPosData);
                  //  }
                }

                if (dictStratWiseNetPositions.TryGetValue(lTrade.lTradeData.Remarks, out lStratNetPos))
                {
                    lStratNetPosData = lStratNetPos.UpdateOnTrade(ref lTrade);
                }
                else
                {
                    lStratNetPos = new StratNetPos();
                    lStratNetPosData = lStratNetPos.UpdateOnTrade(ref lTrade);
                    dictStratWiseNetPositions.Add(lTrade.lTradeData.Remarks, lStratNetPos);
                  //  if (NewNetPosArrived != null)
                   // {
                     //   NewNetPosArrived(ref lContractNetPosData);
                   // }
                }
                
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
            return lContractNetPosData;
            
        }
    }

    internal class TokenNetPos
    {
       
        internal Dictionary<string,NetPosData> dictTokenClientWiseNetPositions;

        internal TokenNetPos()
        {
            dictTokenClientWiseNetPositions = new Dictionary<string, NetPosData>();
        
        }

        internal void UpdateOnTrade(ref NetPosData lNetPosData)
        {
            
            NetPosData lpNetPosData;
            if(dictTokenClientWiseNetPositions.TryGetValue(lNetPosData._clientId, out lpNetPosData))
            {
                lpNetPosData = lNetPosData;
            }
            else
            {
                
                dictTokenClientWiseNetPositions.Add(lNetPosData._clientId, lNetPosData);
            }
        }

      
    }

    internal class StratNetPos
    {
        NetPosData lStratNetPosData;
        NetPosData lStratContractNetPosData;
        public Dictionary<Int32, NetPosData> dictContractWiseNetPositions;
        public delegate void NetPosMessageHandler(ref NetPosData pNetPosData);
        public static event NetPosMessageHandler NewNetPosArrived;

        internal StratNetPos()
        {
            lStratNetPosData = new NetPosData();
            dictContractWiseNetPositions = new Dictionary<Int32, NetPosData>();
        }
        internal NetPosData UpdateOnTrade(ref Trade lTrade)
        {
            NetPosData lContractNetPosData = null;
            try
            {
                lStratNetPosData.UpdateOnTrade(ref lTrade, false,true);

                if (dictContractWiseNetPositions.TryGetValue(lTrade.lTradeData.Token, out lContractNetPosData))
                {
                    lContractNetPosData.UpdateOnTrade(ref lTrade, false,true);
                }
                else
                {
                    lContractNetPosData = new NetPosData();
                    lContractNetPosData.UpdateOnTrade(ref lTrade, false,true);
                    dictContractWiseNetPositions.Add(lTrade.lTradeData.Token, lContractNetPosData);
                 
                }

               
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
            return lContractNetPosData;

        }
    }

    internal class NetPosition
    {
        public static string strDisplayClient = "ALL";
        public static Int32 nDisplayToken = -1;
        
        //Client Wise NetPosition
        internal Dictionary<string, ClientNetPos> dictClientWiseNetPositions = new Dictionary<string, ClientNetPos>();
        internal Dictionary<Int32, TokenNetPos> dictTokenWiseNetPositions = new Dictionary<Int32, TokenNetPos>(); // to be used for MTM Updation
       
        public delegate void CallBackDelegate(string sSymbol, NetPosData oData);
        public static CallBackDelegate MyDataCallBack;

        internal void Clear()
        {
            dictClientWiseNetPositions.Clear();
            dictTokenWiseNetPositions.Clear();
           

        }
        internal void UpdateNetPosOnTrade(ref Trade lTrade)
        {
            try
            {
            ClientNetPos lClientNetPos;
            NetPosData lNetPosData;
                NetPosData lStratNetPosData= null;


                string strRem = lTrade.lTradeData.Remarks;
                if (strRem.Contains("COVERFOREXCEL@"))
                    strRem = strRem.Replace("COVERFOREXCEL@", "STRAT");
                if (strRem.Contains("FULLCOVER"))
                    strRem = strRem.Replace("FULLCOVER", "");
                if (strRem.Contains("COVER"))
                    strRem = strRem.Replace("COVER", "STRAT");

                lTrade.lTradeData.Remarks = strRem;

                if (dictClientWiseNetPositions.TryGetValue(lTrade.lTradeData.ClientId, out lClientNetPos))
            {
                lNetPosData = lClientNetPos.UpdateOnTrade(ref lTrade, ref lStratNetPosData);
                    if (MyDataCallBack != null)
                    {
                        MyDataCallBack(lNetPosData._symbol, lNetPosData);
                        MyDataCallBack(lStratNetPosData._symbol, lStratNetPosData);
                    }
            }
            else
            {
                lClientNetPos = new ClientNetPos();
                lNetPosData = lClientNetPos.UpdateOnTrade(ref lTrade, ref lStratNetPosData);
                dictClientWiseNetPositions.Add(lTrade.lTradeData.ClientId,lClientNetPos);
                    if (MyDataCallBack != null)
                    {
                        MyDataCallBack(lNetPosData._symbol, lNetPosData);
                        MyDataCallBack(lStratNetPosData._symbol, lStratNetPosData);
                    }
              

            }

            TokenNetPos lTokenNetPos;
            if (dictTokenWiseNetPositions.TryGetValue(lTrade.lTradeData.Token, out lTokenNetPos))
            {
                    lTokenNetPos.UpdateOnTrade(ref lNetPosData);
            }
            else
            {
                lTokenNetPos = new TokenNetPos();
                lTokenNetPos.UpdateOnTrade(ref lNetPosData);
                dictTokenWiseNetPositions.Add(lTrade.lTradeData.Token, lTokenNetPos);
            }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
           
        }
           
        
    

        public static void RegisterCallBack(CallBackDelegate AppReceiver1)
        {
            MyDataCallBack = AppReceiver1;
        }

      
    }
}
