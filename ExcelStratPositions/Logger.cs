﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ExcelStratPositions
{
    public static class Logger
    {
        static FileStream fs;
        static StreamWriter sw;
        static Logger()
        {
            string sFileName = string.Format(".\\ExcelStratPos{0}.Log", DateTime.UtcNow.ToString("yyyy-MM-dd"));

            if (!File.Exists(sFileName))
            {
                fs = File.Open(sFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
                sw = new StreamWriter(fs);
            }
            else
            {
                fs = File.Open(sFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
                sw = new StreamWriter(fs);
            }
           

        }

        public static void WriteToLog(string s)
        {
             string sFinalMessage = string.Format("{0}   {1} ss", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff ttt"), s);
             sw.WriteLine(sFinalMessage);
             sw.Flush();
        }
    }
}
