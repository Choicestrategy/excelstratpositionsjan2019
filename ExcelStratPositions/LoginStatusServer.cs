﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelDna.Integration.Rtd;
using ExcelDna.Integration;

namespace ExcelStratPositions
{

    public  class LoginStatusServer: ExcelRtdServer
    {

        
    }
    public static class Loginfunctions 
    {
        [ExcelFunction("Gets realtime values from server")]
        public static object GetLoginStatusStrat()
        {
            return ExcelStratPositions2.bRELogin;
        }

        public static object GetGWLoginStatusStrat()
        {
            return ExcelStratPositions2.bGWLogin;
        }
    }
    
}
