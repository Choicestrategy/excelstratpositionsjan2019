﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ExcelStratPositions
{
    //public class OrderRTDData
    //{
    //    public string InternalOrderId;
    //    public string Symbol;
    //    public Int32 Token;
    //    public Int32 TotalQty;
    //    public Int32 PendingQty;
    //    public Int32 Price;
    //    public String Status;
    //    public String Reason;
    //    public String ExchangeOrderNo;

    //}
    public static class OrderProcessor
    {
        public delegate void CallBackDelegate(string sOrderId, TradeSystemCommon.OrderData oData);
        public static CallBackDelegate MyDataCallBack;
        public static ConcurrentDictionary<string, TradeSystemCommon.OrderData> lOrderDict = new ConcurrentDictionary<string, TradeSystemCommon.OrderData>();
        public static ConcurrentDictionary<string, string> lStratClientDict = new ConcurrentDictionary<string, string>();

        public static object ByteArrayToObject(Byte[] Buffer)
        {

            object rval = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream stream = new MemoryStream(Buffer);
                rval = formatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
            return rval;
        }


        public static void ProcessOrder(string[] strMessageArray)
        {
            string OID = "";
            TradeSystemCommon.OrderData lOrderData = new TradeSystemCommon.OrderData();
            foreach (string str in strMessageArray)
            {
                //CODE = |BS= |DEALER=''|CLI="|INST="|Q=|P=|OT="|VAL="|TP= ""|DQ=""|OID=""|REM=""|Status =""|Reason=""|EntryTime=|OrderTime=|ExchangeOrderId=""|
                string[] strTag = str.Split('=');
                switch (strTag[0])
                {
                    case "OID":
                        {
                            OID = strTag[1];
                        }
                        break;
                }
            }

            if (lOrderDict.TryGetValue(OID, out lOrderData))
            {

            }
            else
            {
                lOrderData = new TradeSystemCommon.OrderData();
                lOrderDict.TryAdd(OID, lOrderData);
            }


                foreach (string str in strMessageArray)
            {
                //CODE = |BS= |DEALER=''|CLI="|INST="|Q=|P=|OT="|VAL="|TP= ""|DQ=""|OID=""|REM=""|Status =""|Reason=""|EntryTime=|OrderTime=|ExchangeOrderId=""|
                string[] strTag = str.Split('=');
                switch (strTag[0])
                {
                    case "OID":
                        {
                            lOrderData.InternalOrderId = strTag[1];
                        }
                        break;
                    case "REM":
                        {
                            lOrderData.Remarks = strTag[1];
                        }
                        break;
                    case "CLI":
                        {
                            lOrderData.ClientId = strTag[1];
                        }
                        break;
                    case "OT":
                        {
                            lOrderData.Validity = strTag[1];
                        }
                        break;
                    case "VAL":
                        {

                            lOrderData.Validity = strTag[1];
                        }
                        break;
                    case "TOTALTRADEDQTY":
                        {
                        }
                        break;
                    case "BS":
                        {
                            lOrderData.BuySell = strTag[1];
                        }
                        break;
                    case "EXGOID":
                        {
                            lOrderData.ExchangeOrderNo = strTag[1];
                        }
                        break;
                    case "P":
                        {
                            lOrderData.Price = Convert.ToInt32(strTag[1]);
                        }
                        break;
                    case "REASON":
                        {
                            lOrderData.Reason = strTag[1];
                        }
                        break;
                    case "STATUS":
                        {
                            lOrderData.Status = strTag[1];
                        }
                        break;
                    case "PENDINGQTY":
                        {

                        }
                        break;
                    case "Q":
                        {
                            lOrderData.Qty = Convert.ToInt32(strTag[1]);
                        }
                        break;
                }
            }
                        

            string strCli = "";
            if (!lStratClientDict.ContainsKey(lOrderData.Remarks))
                lStratClientDict.TryAdd(lOrderData.Remarks, lOrderData.ClientId);

            if (MyDataCallBack != null)
                MyDataCallBack(lOrderData.InternalOrderId, lOrderData);

        }

        public static void ProcessTradeOrder(ref string[] strMessageArray)
        {
            try
            {
                //if (lOrderDict.TryGetValue(lOrderData.InternalOrderId, out lOrderData1))
                //CODE=OCONF|BS=B|DEALER=DLR55|CLI=A85|INST=FUTSTK-INFY-29JUN2017|Q=500|P=92000|OT=RL|VAL=IOC|TP=|DQ=|OID=DLR55-A85-7|REM=||ROE|STATUS=CONFIRMED|REASON=|OTIME=1183206739|EXGOID=1300000004147993|TS1=18054|TS2=1297349645
                string OID = "";
                int PendingQty = 0;
                int TotalQty = 0;
                int TotalTradedQty = 0;
                string Status;
                string REM="";
                string CLient="";

                foreach (string str in strMessageArray)
                {
                    //  {
                    //CODE = |BS= |DEALER=''|CLI="|INST="|Q=|P=|OT="|VAL="|TP= ""|DQ=""|OID=""|REM=""|Status =""|Reason=""|EntryTime=|OrderTime=|ExchangeOrderId=""|
                    string[] strTag = str.Split('=');
                    switch (strTag[0])
                    {
                        case "TOTALQTY":
                            {
                                TotalQty = Convert.ToInt32(strTag[1]);
                            }
                            break;
                        case "Status":
                            {

                                // Status = strTag[1];
                            }
                            break;
                        case "TOTALTRADEDQTY":
                            {
                                TotalTradedQty = Convert.ToInt32(strTag[1]);
                            }
                            break;
                        case "LASTUPDATEIME":
                            {
                                //LastUpdateTime = Convert.ToInt32(strTag[1]);
                            }
                            break;
                        case "PENDINGQTY":
                            {
                                PendingQty = Convert.ToInt32(strTag[1]);
                            }
                            break;
                        case "OID":
                            {
                                OID = strTag[1];
                            }
                            break;
                        case "REM":
                            {
                                REM = strTag[1];
                            }
                            break;
                        case "CLIENT":
                            {
                                CLient = strTag[1];
                            }
                            break;
                    }


                }
                if (TotalTradedQty == TotalQty)
                    Status = "TRADED";
                else
                    Status = "PART TRADE";
                //if (Side.Equals("S"))
                //  OQty = -OQty;

                // if (Program.lDictBasePrice.ContainsKey(OrdId))
                //   BASEPRICE = Program.lDictBasePrice[OrdId];
                TradeSystemCommon.OrderData lOrderData1;
                if (lOrderDict.TryGetValue(OID, out lOrderData1))
                {
                    if (TotalTradedQty > lOrderData1.TotalTradedQty)
                    {
                        lOrderData1.PendingQty = PendingQty;

                        lOrderData1.TotalTradedQty = TotalTradedQty;
                        lOrderData1.Status = Status;
                    }
                }

                if (!lStratClientDict.ContainsKey(REM))
                    lStratClientDict.TryAdd(REM, CLient);


                if (MyDataCallBack != null)
                    MyDataCallBack(lOrderData1.InternalOrderId, lOrderData1);
            }
            catch(Exception e)
            {

            }
            return ;
        }
        public static void ProcessOrder(byte[] b)
        {
            try
            {
                if (b != null)
                {
                    Object obj = ByteArrayToObject(b);
                    TradeSystemCommon.OrderData lOrderData = (TradeSystemCommon.OrderData)obj;

                    TradeSystemCommon.OrderData lOrderData1;
                   // if(lOrderData.InternalOrderId == "DLR60-M68-COVER2-05082018-19512574450-ITC")
                    {

                    }
                    if (lOrderDict.TryGetValue(lOrderData.InternalOrderId, out lOrderData1))
                    {
                        lOrderDict[lOrderData.InternalOrderId] = lOrderData;
                    }
                    else
                        lOrderDict.TryAdd(lOrderData.InternalOrderId, lOrderData);

                    string strCli="";
                    if (!lStratClientDict.ContainsKey(lOrderData.Remarks))
                        lStratClientDict.TryAdd(lOrderData.Remarks, lOrderData.ClientId);

                    if (MyDataCallBack != null)
                        MyDataCallBack(lOrderData.InternalOrderId, lOrderData);
                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
        }

        public static void ProcessOrderDownload(byte[] b)
        {
            try
            {
                if (b != null)
                {


                    int nLength = 0;
                    int nPacketLength = 0;
                    byte[] b2 = new Byte[2];
                    byte[] b3 = null;
                    while (nLength < b.Length)
                    {
                        Buffer.BlockCopy(b, nLength, b2, 0, 2);
                        nPacketLength = BitConverter.ToInt16(b2, 0);
                        nLength += 2;
                        b3 = new byte[nPacketLength];
                        Buffer.BlockCopy(b, nLength, b3, 0, nPacketLength);
                        nLength += nPacketLength;
                        ProcessOrder(b3);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }

        }
        //public static void ProcessOrder(string[] sMessageArray)
        //{
        //    OrderRTDData oData = new OrderRTDData();
        //    foreach (string str in sMessageArray)
        //    {
        //        string[] strTag = str.Split('=');
        //        //CODE = |BS= |DEALER=''|CLI="|INST="|Q=|P=|OT="|VAL="|TP= ""|DQ=""|OID=""|REM=""|Status =""|Reason=""|EntryTime=|OrderTime=|ExchangeOrderId=""|
        //        strTag = str.Split('=');

        //        switch (strTag[0])
        //        {
        //            case "Q":
        //                {

        //                     Int32.TryParse(strTag[1], out oData.TotalQty);
                            
        //                }
        //                break;
        //            case "P":
        //                {
        //                     Int32.TryParse(strTag[1], out oData.Price);
                            
        //                }
        //                break;
        //            case "OID":
        //                {
        //                    oData.InternalOrderId = strTag[1];
                           
        //                }
        //                break;
        //            case "STATUS":
        //                {
        //                    oData.Status = strTag[1];
        //                }
        //                break;
        //            case "REASON":
        //                {
        //                    oData.Reason = strTag[1];
        //                }
        //                break;
        //            case "EXGOID":
        //                {
        //                    oData.ExchangeOrderNo = strTag[1];
        //                }
        //                break;

        //        }
        //    }

        //    MyDataCallBack(oData.InternalOrderId, oData);
        //}

        public static void RegisterCallback(CallBackDelegate AppReceiver1)
        {
            MyDataCallBack = AppReceiver1;
        }
    }
}
