﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ExcelStratPositions
{
    public class MyXMLSettings
    {
        public string strPath;
        public string strLogin;
        public string strPassword;
        public string strServerIP;
        public int strServerPort;
        public string strbatchfile;
        public string strBackFillDays;
        public string strVersionNo;
        public int iLog;
        public string strContinuity;
        public MyXMLSettings()
        {
            try
            {

                strVersionNo = "2";
                XElement xeleSettings = XElement.Load(".\\SettingsStratNetPos.xml");
                strServerIP = xeleSettings.Descendants().Where(c => c.Attribute("IPAddress").Name == "IPAddress").Select(c => c.Attribute("IPAddress").Value).FirstOrDefault();
                String strServerPort1 = xeleSettings.Descendants().Where(c => c.Attribute("Port").Name == "Port").Select(c => c.Attribute("Port").Value).FirstOrDefault();
                strServerPort = Convert.ToInt16(strServerPort1);
                iLog = Convert.ToInt16(xeleSettings.Descendants().Where(c => c.Attribute("CommFileLog").Name == "CommFileLog").Select(c => c.Attribute("CommFileLog").Value).FirstOrDefault());
                strLogin = xeleSettings.Descendants().Where(c => c.Attribute("LoginID").Name == "LoginID").Select(c => c.Attribute("LoginID").Value).FirstOrDefault();
                strPassword = xeleSettings.Descendants().Where(c => c.Attribute("Password").Name == "Password").Select(c => c.Attribute("Password").Value).FirstOrDefault();
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.ToString());
            }
        }



    }
}
