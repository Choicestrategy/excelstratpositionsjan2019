﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using TradeSystemCommon;
//using MySql.Data.MySqlClient;

namespace ExcelStratPositions
{


    internal class Trade
    {
       

         internal TradeData lTradeData;


         internal Trade()
        {
            lTradeData = new TradeData();
        }
        internal bool FillTrade(ref string[] strMessageArray)
        {
            //string strMessage = "";
            try
            {


                foreach (string str in strMessageArray)
                {
                    //CODE = |BS= |DEALER=''|CLI="|INST="|Q=|P=|OT="|VAL="|TP= ""|DQ=""|OID=""|REM=""|Status =""|Reason=""|EntryTime=|OrderTime=|ExchangeOrderId=""|
                    string[] strTag = str.Split('=');
                    switch (strTag[0])
                    {
                      

                        case "TRDQTY":
                            {

                                bool b = Int32.TryParse(strTag[1], out lTradeData.Qty);
                                if (b == false)
                                {
                                   // strMessage += "Qty should be a number,";
                                    return false;
                                }
                                else if (lTradeData.Qty < 0)
                                {
                                   // strMessage += "Qty cannot be negative";
                                    return false;
                                }
                            }
                            break;
                        case "TRDPRC":
                            {
                                bool b = Int32.TryParse(strTag[1], out lTradeData.Price);
                                if (b == false)
                                {
                                    return false;
                                    // return strMessage;
                                }
                                else if (lTradeData.Price < 0)
                                {
                                    return false;
                                }
                            }
                            break;


                        case "OID":
                            {
                                lTradeData.InternalOrderId = strTag[1];
                                if (String.IsNullOrEmpty(lTradeData.InternalOrderId))
                                {
                                    return false;
                                }
                            }
                            break;
                        case "TRDNO":
                            {
                                bool b = Int32.TryParse(strTag[1], out lTradeData.TradeNo);
                                if (b == false)
                                {
                                    return false;
                                    //return strMessage;
                                }
                                else if (lTradeData.TradeNo < 0)
                                {
                                    return false;
                                }
                            }
                            break;
                        case "TTIME":
                            {
                                bool b = Int32.TryParse(strTag[1], out lTradeData.TradedTime);
                                if (b == false)
                                {
                                    //return false;
                                    // return strMessage;
                                }
                            }
                            break;
                        case "DEALER":
                            {
                                lTradeData.DealerId = strTag[1];
                            }
                            break;
                        case "CLIENT":
                            {
                                lTradeData.ClientId = strTag[1];
                                
                            }
                            break;
                        case "BUYSELL":
                            {
                                lTradeData.BuySell = strTag[1];
                               
                            }
                            break;
                        case "TOKEN":
                            {
                                bool b = Int32.TryParse(strTag[1], out lTradeData.Token);
                                if (b == false)
                                {
                                    return false;
                                    // return strMessage;
                                }
                            }
                            break;
                        case "REM":
                            {
                                lTradeData.Remarks = strTag[1];
                            }
                            break;
                    }
                }

            
              OrderData lOrderData;
                if (lTradeData.InternalOrderId != null)
                {
                   // if(lTradeData.InternalOrderId == "DLR60-M68-COVER2-05082018-19512574450-ITC")
                    if (OrderProcessor.lOrderDict.TryGetValue(lTradeData.InternalOrderId, out lOrderData))
                        lTradeData.Remarks = lOrderData.Remarks;
                }

              //  lTradeData.ExchangeOrderNo = lOrder.lOrderData.ExchangeOrderNo;


                    //  TimeSpan span = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
                    // lTradeData.LastUpdateTime = Convert.ToInt32(span.TotalSeconds);     


            }
            catch (Exception e)
            {
                Logger.WriteToLog("Fill Trade");
            
               Logger.WriteToLog(e.ToString());
            }

            if (String.IsNullOrEmpty(lTradeData.BuySell))
                return false;

                    
           return true;
        }
    }
     
    
}
